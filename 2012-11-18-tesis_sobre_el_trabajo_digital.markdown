---
layout: post
author: "Michel Bauwens"
title: "Tesis sobre el trabajo digital"
license: http://creativecommons.org/licenses/by-sa/3.0/
cover: "images/covers/en-defensa-del-software-libre-2.png"
category: [ "En Defensa del Software Libre #2" ]
---

<https://p2p-left.gitlab.io/statement/>
