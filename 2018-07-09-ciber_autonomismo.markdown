---
layout: post
author: Paolo Gerbaudo
title: "Del ciber-autonomismo al ciber-populismo: una historia de la ideología del activismo digital"
cover: "images/covers/en-defensa-del-software-libre-5.png"
license: https://endefensadelsl.org/ppl_deed_es.html
---

> [From Cyber-Autonomism to Cyber-Populism: An Ideological Analysis of > the
> Evolution of Digital
> Activism](https://www.triple-c.at/index.php/tripleC/article/view/773) publicado
> en Triple-C Volumen 15.  Traducido y liberado bajo licencia de producción de
> pares con permiso del autor.


Introducción
------------

La expresión "Activismo digital" es ampliamente utilizada para describir
distintas formas de activismo que se sirven de la tecnología digital
que han sido atravesadas por una rápida transformación desde su
emergencia en el amanecer de la Internet.  En la actualidad es posible
identificar dos grandes olas.  La primera se corresponde con la
popularización temprana de la Internet y el auge de la Web a mediados de
los '90.  Esta ola abarcó una cantidad de proyectos e iniciativas
impulsadas por las activistas de la tecnología y los medios alternativos
que se enmarcaban en el movimiento anti-globalización, incluyendo el
sitio de noticias alternativas _Indymedia_, las listas de correo alternativas, 
grupas hackers (o _hacktivistas_) y laboratorios hacker (o _hacklabs_).  
La segunda ola coincide con el auge de la así llamada _Web 2.0_, los sitios de 
redes sociales como _Facebook_, _YouTube_ y _Twitter_, que fueron acompañadas 
por el surgimiento de los famosos colectivos hacker _Anonymous_ y _Lulzsec_;
así como por el "activismo de medios sociales" del 15M, _Ocuppy_ y otros
movimientos de las plazas, cuyas organizadoras utilizaron los sitios de
medios sociales como plataformas para la movilización de masas.  ¿En qué
medida estas dos fases del activismo digital son simples reflejos de la
evolución de la tecnología digital y del salto de la _Web 1.0_ a la _Web
2.0_ (como usualmente se las describe)?  ¿Debemos entender sus
diferencias como derivadas de los cambios en las posibilidades
materiales de una tecnología digital en un momento de rápida innovación
o hay que agregar algo más a la ecuación?

Hasta el momento, el debate sobre la transformación del activismo
digital ha tendido a seguir la típica tendencia tecno-determinista que
considera a la tecnología como la causa última de la transformación
social.  Esta concepción es sostenida por la popularidad que han tomado
términos como "revolución 2.0" [@ghonim-2012],
"wiki-revolución" [@ferron-massa-2012] o
"twitter-revolución" [@morozov-2009], utilizados ampliamente en los
medios y estudios académicos para referirse a los movimientos de
protesta que se sirven de la tecnología digital.  La racionalidad
subyacente en estas expresiones es que la adopción de un cierto tipo de
plataforma, como _Facebook_ o _Twitter_, define automáticamente la forma
de activismo que se manifiesta a través de ellas.  Este abordaje
proviene de una visión simplista de los efectos de la tecnología que está
profundamente enraizada en la teoría de medios de McLuhan y su famosa
frase "el medio es el
mensaje" [@mcluhan-fiore-1967;@mcluhan-gordon-lamberti-scheffel-dunand-2011] y
según la cual, el uso de un determinado dispositivo tecnológico resulta
en una serie de consecuencias inevitables.  La escuela de la ecología de
medios basada en las obras de McLuhan tiene cosas muy importantes para
decir sobre la forma en que la tecnología estructura la acción, por
ejemplo la forma en que las diferentes tecnologías de la comunicación
(como el teléfono, la TV o Internet) traen consigo distintas
arquitecturas comunicacionales (una-a-una, una-a-muchas,
muchas-a-muchas) y diferentes disposiciones para las usuarias de esas
tecnologías  [@postman-1985;@lundby-2009].  Sin embargo, tiende a
descuidar los factores no-tecnológicos --los socio-económicos, políticos
y culturales-- que intervienen en la definición del contenido del
activismo.  Para superar esta visión simplista de la tecnología como una
fuerza no-mediada capaz de dar su propia forma a las estructuras
organizacionales y las prácticas de protesta, el análisis del activismo
digital necesita recuperar una comprensión de la ideología, entendida
como una cosmovisión y un sistema de valores que da forma a la acción
colectiva y de cómo la ideología interactúa con la tecnología para dar
forma a las prácticas activistas.

Siguiendo este abordaje, en este artículo desarrollo una periodización
del activismo digital que se centra alrededor de dos olas, cada una con
sus características ideológicas y sus orientaciones "tecno-políticas"
propias --para usar el término introducido por Rodotà para describir el
nexo entre la política y la tecnología--, adoptado tanto por activistas
como por investigadoras.  Para este fin tomaré elementos de mi trabajo
previo [-@gerbaudo-2012;-@gerbaudo-2016] en el movimiento de las plazas
del 2011 y otros movimientos posteriores.

El argumento puede ser resumido esquemáticamente así: las activistas
anti-globalización adoptaron un abordaje tecno-político al que describo
como ciber-autonomista, enraizado en la contra-cultura de los '70 y '80,
la cultura DIY y la tradición de los medios alternativos; desde las
radios piratas hasta los _fanzines_.  Estas diferentes inspiraciones
compartían un énfasis en la lucha por la liberación de las personas y
las comunidades locales de la interferencia de las instituciones de
mayor escala.  Con estos antecedentes, el ciber-autonomismo tomó la
Internet como un espacio autónomo.  El movimiento de las plazas, en
cambio ha adoptado una actitud ciber-populista que ve a la Internet como
un espacio de movilización de masas donde las personas individuales se
unen en una subjetividad inclusiva y sincrética.  Este abordaje refleja
el giro populista que ha marcado el movimiento de las plazas, como la
adopción de un discurso del pueblo o del 99% contra las
élites [@gerbaudo-2017].

Estas dos orientaciones tecno-políticas reflejan el proceso de evolución
tecnológica que fue desde la más elitista _web 1.0_, a la masificada
_web 2.0_ con sus sitios de redes sociales.  Pero para analizarlas no es
cuestión de reducirlas a la mera transformación tecnológica, también
hace falta abarcar una pluralidad de otros factores y tomar en cuenta el
cambio abismal en las actitudes y percepciones causadas por la crisis
financiera de 2008 y los desarrollos ideológicos relacionados.  En
paralelo al giro de los movimientos sociales desde el anarco-autonomismo al
populismo como forma dominante de la ideología contestataria, el
activismo digital ha transicionado de tomar a Internet como un espacio
de resistencia y contestación contra-cultural, hacia un espacio de
movilización contra-hegemónica.

El artículo comienza como una discusión teórica sobre los diferentes
factores involucrados en la transformación del activismo digital y en
particular, sobre la relación entre tecnología, política y cultura.  Se
subraya la necesidad de prestar más atención a los factores políticos,
culturales e ideológicos para comprender el activismo digital más allá
del tecno-determinismo que actualmente domina su cobertura.  Intento
demostrar cómo los cambios ideológicos han dado forma a la
transformación del activismo digital, explorando la transición del
ciber-autonomismo al ciber-populismo y cómo se ha manifestado en algunos
ejemplos concretos.  Concluyo con algunas reflexiones sobre las
implicancias de investigaciones futuras sobre el activismo digital,
enfatizando la necesidad de traer la ideología de vuelta al análisis de
los movimientos de protesta en la era digital.

La tecno-política más allá del tecno-determinismo
-------------------------------------------------

El activismo digital es una forma de activismo que pone en el eje de su
discusión la relación entre política y tecnología.  Para hacer uso de un
término en boga entre las activistas e investigadoras en los últimos
años, se trata de la naturaleza y la dinámica de la "tecno-política".
"Tecno-política" es un término acuñado por el político y académico
italiano Stefano Rotodà [-@rodota-1997] para referirse al nexo entre la
política y la tecnología, que desde entonces ha sido popularizado por
académicas y activistas (como Javier Toret [-@toret-2013] en España) para
definir el nuevo campo de análisis que enmarca el activismo digital.  Al
referirnos a los dos conceptos constitutivos de la tecno-política,
podemos argumentar que hasta este momento los estudios sobre el
activismo digital se han enfocado excesivamente en la tecnología antes
que en la política.  Las académicas han tendido a leer la transformación
política como el resultado de la transformación tecnológica y por lo
tanto, han soslayado que lo inverso también es apropiado, es decir, que los
cambios políticos e ideológicos modifican la forma en que la tecnología
es concebida y utilizada.

La naturaleza tecno-determinista de gran parte de la academia
contemporánea sobre el activismo digital propone que la naturaleza de
esta forma de activismo se deriva directamente de propiedades
específicas de la tecnología.  Esto puede verse en el debate respecto a los
efectos de la cobertura mediática sobre el activismo digital.  Un
ejemplo es el libro de Earl y Kimport [-@earl-kimport-2011] y la forma en
que aborda los medios digitales como un grupo de aparatos que reducen
los costos de participación y por lo tanto facilitan nuevas formas de
interacción que anteriormente eran imposibles.  En sintonía con gran
parte de la literatura proveniente de las ciencias políticas, esta
perspectiva propone una comprensión instrumental y económica de los
efectos de los medios, como puede verse en el lenguaje de los "costos" y
"beneficios" que utilizan para explicar el uso de la tecnología digital.
Este abordaje explica las ventajas prácticas que constituye la tecnología 
digital para las activistas, pero omite la dimensión simbólica y cultural del
activismo digital, empezando por el mismo contenido de aquello que es
canalizado a través de esa tecnología.  Puede hacerse una crítica
similar a la obra de Lance W. Bennett y Alexandra Sederberg.  Su teoría
describe una "acción conectiva" [@bennett-sederberg-2012] en oposición a
la noción de la acción colectiva.  Argumentan que los _social media_,
con su capacidad de promover la conectividad, superan la lógica
colectiva de los movimientos sociales anteriores y su necesidad de
liderazgo e identidad colectiva [@bennett-sederberg-2012].  Gracias a la
tecnología digital, los movimientos pueden volverse más personalizados y
menos controlados por centros organizacionales.  Pero lo que se soslaya
en este contexto es que la aplicación liberadora de la tecnología
digital está muy lejos de ser el resultado inevitable.  Las
potencialidades de la tecnología digital pueden volverse hacia objetivos
políticos totalmente distintos y acoplarse a diferentes formatos
organizacionales.  Resulta suficiente pensar que por ejemplo, fenómenos
políticos tan dispares como _Occupy Wall Street_ y la campaña
presidencial de Donald Trump, se hayan servido tan eficientemente de los
_social media_, aun en formas y estructuras organizacionales
radicalmente distintas.

Este elemento tecno-determinista también se encuentra presente en la
obra de Manuel Castells.  Para ser justas, el registro de Castells tiene
muchos más matices que los puramente estructuralistas que provienen del
resto de las ciencias políticas.  Esto es así porque Castells trabaja
desde la tradición sociológica y su abordaje también toma en cuenta los
factores culturales que se ponen en juego en la Internet y el activismo
digital.  A diferencia de otras autoras, no ve a la tecnología como un
monolito todopoderoso, sino también como un producto social y cultural.
Desde este punto de vista Castells ha argumentado que un factor
importante para comprender la cultura digital es la influencia del
espíritu libertario de los movimientos de protesta de los '60 y '70 y la
forma en que inspiraron la arquitectura distribuida de la
Internet [@castells-2004].  Sin embargo, su teoría de la sociedad-red y
su concepción sobre la tecnología digital como apartada de la estructura
piramidal de la sociedad fordista hacia unas estructuras de red
apropiadas a la sociedad de la información, todavía contiene algunos
elementos tecno-deterministas.  La tecnología provoca un cambio
"morfológico" que atraviesa a toda la sociedad y tiene consecuencias en
todos los campos y organizaciones que adoptan la tecnología digital.
Esta perspectiva sin duda contiene un elemento de verdad, pero parece
omitir la flexibilidad con la que los procesos organizacionales son
influenciados.  Es un error asumir que la tecnología digital tiende a
erosionar las jerarquías.  Como he demostrado en mis obras anteriores,
el activismo digital no es un espacio horizontal sin líderes, sino que
está acompañado por nuevas formas de
liderazgo [-@gerbaudo-2012;-@gerbaudo-2016].

Esta tendencia también puede observarse en las obras de Castells sobre
los _social media_.  Castells argumenta que la difusión de _social_
_media_ como _Facebook_ y _Twitter_ ha transformado la comunicación en
Internet  y ha introducido una nueva lógica mediática a la que describe como
"auto-comunicación de masas" [@castells-2009], que combina la lógica de
la auto-comunicación de los medios personales una-a-una --como el
teléfono-- con las masas y la capacidad una-a-muchas de los medios
masivos.  Según Castells, esta lógica comunicacional estuvo en la base
de los movimientos del 2011 como los indignados, _Occupy_ y la primavera
árabe y contribuyó fuertemente a su alcance masivo [@castells-2012].
Esto ciertamente provee razones poderosas para comprender la forma en
que la segunda ola del activismo digital ha superado las políticas
minoritarias de la primera ola.  Los _social media_ proveyeron las
condiciones técnicas necesarias para que emerjan las nuevas formas de
activismo.  No obstante, Castells tiende a omitir cómo han convergido
factores ideológicos y políticos en este cambio.  Como veremos en el
curso de este artículo, sin un cambio ideológico las nuevas
oportunidades de movilización de masas ofrecidas por los _social media_
no hubieran podido ser cosechadas por los movimientos de protesta.

La obra de Jeffrey Juris, antropólogo y alumno de Manuel Castells, ha
seguido una línea similar de razonamiento, leyendo la transformación del
activismo como resultado de la transformación tecnológica.  En su
influyente libro _Networking Futures_ \[Los futuros en
red\] [-@juris-2008], Juris argumenta que el movimiento
anti-globalización se basaba en un imaginario de la red que constituyó
la inspiración clave de una cantidad de proyectos de activismo digital
que emergieron en ese tiempo, incluyendo el sitio de noticias
alternativo _Indymedia_ y las listas de correo alternativas utilizadas
por las activistas para organizar actividades y campañas específicas.
En su obra sobre el movimiento de las plazas de 2011, Juris dice que
esta ola tuvo una lógica diferente a la de la anti-globalización.
Argumenta que hubo un cambio de una lógica de red de las activistas
anti-globalización hacia lo que describe como una "lógica de
agregación".  Esta transformación deriva de la evolución de la _Web 1.0_
hacia la _Web 2.0_ y esta lógica de agregación refleja las nuevas
potencialidades de difusión masiva de las plataformas de _social media_.
Esta lógica es apoyada por la "viralidad", es decir la capacidad para la
difusión rápida permitida por las redes sociales corporativas como
_Facebook_ y _Twitter_.  Esta capacidad ha sido trasladada físicamente a
las plazas ocupadas de 2011, rebosantes de grandes
multitudes [@juris-2012].  El inspirador análisis de Juris provee algunas
ideas interesantes sobre el apuntalamiento tecnológico que encontramos
en la transformación de las tácticas de protesta.  Aun así, omite cómo
este cambio en la forma de protestar también está basado en cambios
significativos en la cultura e ideología de las protestas.

Recuperando la cultura de la protesta
-------------------------------------

Mientras estos abordajes están en lo cierto al identificar la influencia
que juega la tecnología en la política contemporánea, a menudo tienden a
adoptar una posición reduccionista en esta relación causa-efecto, donde
un cierto tipo de arreglo tecnológico lleva automáticamente a una cierta
lógica de acción y se presta poca atención al proceso de mediación
política o cultural que interviene en los diferentes ejemplos concretos
de activismo digital.  En efecto, el activismo digital no es solo un
fenómeno técnico, es un fenómeno.  Es una actividad que involucra la
comunicación de ciertos mensajes, ideas, imágenes y por lo tanto, está
compuesto no solo de una dimensión tecnológica, sino también de una
cultural.  La naturaleza cultural, así como más generalmente política
del activismo digital debe ser tomada en cuenta para comprender por qué
se ha desarrollado de esta manera y por qué ha cambiado a través del
tiempo.  Para superar el sesgo tecno-determinista de los debates
contemporáneos es necesario prestar atención a la compleja imbricación
de la política, la cultura y la tecnología, con referencias específicas
a a) la autonomía relativa de la política respecto de la tecnología; b)
el carácter simbólico y no solo material de los procesos tecnológicos;
c) el rol de la tecnología como mediadora de relaciones sociales y
formas de vida que no pueden ser reducidas a la tecnología misma.

En primer lugar, un problema clave en los abordajes tecno-deterministas
es la forma en que la tecnología es vista como una variable
independiente siempre obligada a determinar la lógica de acción de los
movimientos sociales y en consecuencia, como algo que debe ser dirigido 
en una cierta dirección.  Este acercamiento descuida lo que puede describirse 
como "la autonomía relativa de los procesos políticos y culturales de la
tecnología", es decir la forma en que la cultura y la política son
influenciadas pero no reducibles a la tecnología.  La tecnología no
define por sí misma el activismo, sino que el activismo está siempre
informado por los contenidos culturales que canaliza, por las ideas,
imágenes y puntos de vista que promueve.  Una cantidad de obras recientes
ilustran este punto.

En su libro _CyberLeft_ \[CiberIzquierda\], Wolfson observa el
movimiento anti-globalización y su uso de los medios digitales y resalta
cómo las prácticas asociadas son acompañadas por un _ethos_ y "lógica
cultural" que aborda la Internet no solo como una herramienta, sino
también como un espacio de solidaridad donde luchas diferentes pueden
unirse [-@wolfson-2014].  Barassi y Treré argumentan de forma similar que
además de la evolución tecnológica, resulta importante tomar en cuenta la
experiencia vivida por las activistas que utilizan esas tecnologías y la
forma en que deconstruyen presunciones acerca de la naturaleza y el
propósito de la tecnología [-@barassi-trere-2012].  Coleman argumenta que
el _hacking_ no es solo una práctica técnica, también es social y
conlleva éticas y estéticas específicas, es decir, aspectos que son
influenciados por la tecnología pero que no pueden ser reducidos a
ellos [-@coleman-2013].  Esto puede observarse en la forma en que las
grupas hacker construyen su propio lenguaje y simbología, cuyo epítome
es la máscara de _Anonymous_ tomada de la película de culto _V for
Vendetta_ \[V de Vendetta\].  Por lo tanto es necesario prestar atención
no solo a los dispositivos técnicos usados por las activistas, sino
también a los contenidos culturales que canalizan a través de esas
tecnologías.

En segundo lugar, es importante tomar en cuenta el hecho de que la
tecnología no es solo un aparato material, una estructura técnica o
instrumental con propiedades determinadas.  Es también un objeto
simbólico con significados y usos culturales asociados.  Este es un
aspecto que ha sido ampliamente documentado en la literatura sobre la
domesticación de los medios y la tecnología [@berker-hartmann-punie-2005]
y en los estudios culturales sobre ciencia y
tecnología [@menser-aronowitz-1996; @van-loon-2002].  Las académicas han
mostrado que las tecnologías pueden asociarse a diferentes significados
dependiendo de los contextos sociales y culturales que las utilizan.
Como demuestra Kavada, el activismo digital refleja las propiedades de la
Internet como conjunto de dispositivas técnicas y también a las culturas
que han emergido dentro suyo, como la cultura hacker [-@kavada-2013].  La
Internet no es solo una tecnología, también es un espacio cultural y
resulta difícil separar ambos aspectos.  Esto llama a la necesidad de
explorar el rol que juegan las distintas culturas y subculturas de
Internet y su influencia sobre el activismo digital.

En tercer lugar, deberíamos evitar tener una visión instrumental de la
tecnología en tanto herramienta en sí misma y en su lugar apreciar la
forma en que la tecnología media relaciones sociales, ya que en última
instancia es la forma más importante en la que la tecnología tiene un
efecto sobre los fenómenos sociales.  Este abordaje es central al que
hicieron Marx y Engels de la tecnología industrial.  Lo que importaba no
solo era la forma en que habilitó nuevas formas de producción, también
era el hecho de que materializó una relación de opresión, el de la
burguesía sobre el proletariado [-@marx-engels-2002].  El análisis
tecno-determinista tiende a suspender este aspecto, pasando por alto el
hecho de que la tecnología es una mediadora de cierta relación social,
ya sea de opresión, liderazgo o cooperación.  Aún más, pasa por alto la
forma en que la tecnología se incrusta en ecologías sociales más amplias
(no solo de la comunicación) y en las relaciones sociales que se
establecen dentro de estas.

Lim ha demostrado cómo la efectividad de los _social media_
para circular información relevante a los movimientos de protesta que
eventualmente llevaron a las protestas en la plaza Tahrir en 2011 fue la
presencia de densas redes sociales _offline_.  Esto está ejemplificado
en la forma en que las taxistas del Cairo facilitaron la circulación de
información a través del boca a boca, repitiendo lo que habían escuchado
decir a pasajeras anteriores sobre "lo que se anda diciendo en
_Facebook_" [-@lim-2012].  Los efectos de la tecnología dependen entonces
de lo que permiten y también de las relaciones sociales y las formas de
vida en las que se enredan.  Este aspecto resalta la necesidad de apreciar
la incrustación de la tecnología en diferentes comunidades culturales y
la forma en que el uso tecnológico depende de las costumbres, valores y
normas que esas comunidades adoptan.

Estas críticas piden por un abordaje matizado de la relación entre
tecnología y política, que trate sobre cómo la tecnología influencia la
política y como a su vez la política influencia a la tecnología.  La
forma de alcanzar este objetivo es resucitar la noción de ideología,
entendida en un sentido neutral como un sistema de valores y creencias
adoptado por actoras políticas y sociales que les permiten actuar
colectivamente.  Ideología es un término que provee una forma de
explorar la compleja imbricación de factores políticos, culturales y
sociales que junto a la tecnología influencian cómo se practica el
activismo digital.

Algunas académicas han comenzado a explorar cómo las diferentes
prácticas tecnológicas conllevan sus propias ideologías.  Por ejemplo,
Turner argumenta que el desarrollo de la ciber-cultura estuvo basado en
la ideología del tecno-utopismo y el
tecno-libertarianismo[^libertarian], a su vez influenciadas por la
contra-cultura de los '70 y '80, poniendo énfasis en la auto-realización
individual y la sospecha en las instituciones de gran escala
[-@turner-2010].  Barbrook y Cameron argumentaban que el auge de la
economía digital en los '90 manifestaba una ideología rudimentaria que
describieron como la ideología californiana: una cosmovisión
tecno-_libertarian_ asociando _yuppies_[^yuppie] con _hippies_
[-@barbrook-cameron-1995].  Un elemento ideológico es claramente visible
en los _social media_, que no son solo un conjunto de aplicaciones con
una determinada capacidad material.  Como otros medios, poseen sus
propias ideologías mediáticas [@gershon-2010].  En este caso lo que
podríamos llamar "la ideología de los _social media_" se manifiesta en
el lenguaje de compartir, de explotación multitudinaria
\[_crowd-sourcing_\], de solicitudes de amistad y colaboración que han
introducido [@fuchs-2013;@lovink-2011;van-dijck-2013].  Sobre esta
literatura acerca del nexo entre tecnología e ideología, desarrollo una
periodización del activismo digital separada en dos olas con
características ideológicas distintivas y "orientaciones
tecno-políticas" conectadas, es decir formas ideológicas distintas de
concebir la relación entre política y tecnología.

De 1990 al 2010: El activismo digital desde la contra-cultura a la contra-hegemonía
-----------------------------------------------------------------------------------

Al poner la transformación del activismo digital a través de la lente de
la ideología podemos apreciar la forma en que los factores políticos y
culturales se combinan con los tecnológicos para dar forma al contenido
de varias formas de activismo que se canalizan a través de los _social_
_media_.  Al ser una forma de activismo profundamente entramada con la
tecnología, el activismo digital refleja la naturaleza y la
transformación del ecosistema de comunicaciones digitales [@trere-2012].
Sin embargo, esta influencia tecnológica es "filtrada" por una cantidad
de factores políticos y culturales y más específicamente "orientaciones
tecno-políticas" que determinan cómo una cierta tecnología es concebida
y utilizada.  Esta concepción de la tecnología que describo con el
término orientación tecno-política, tiene un carácter altamente
ideológico ya que involucra el punto de vista de los valores sobre
Internet y su rol en la sociedad y la política.  Sus consecuencias
también son ideológicas ya que son guías para la acción colectiva.

Siguiendo esta línea de pensamiento necesitamos explorar cómo los
procesos evolutivos del activismo digital que normalmente se entienden
como simples frutos de la evolución de la tecnología, de hecho reflejan
un cambio en la ideología de los movimientos de protesta y su posición
tecno-política.  Este puede verse más claramente en el "activismo 1.0"
seguido por el "activismo 2.0" en paralelo con la transición entre "web
1.0" y "web 2.0" y reflejando el cambio tecnológico y sus capacidades.
Resulta obvio que hay algo de verdad en este paralelismo.  No
obstante, intentaré demostrar que las causas de esta transformación
son más complejas y no pueden ser reducidas solo a factores
tecnológicos.  En efecto, además de coincidir con dos olas de la
evolución tecnológica, estas dos olas de activismo digital también
coinciden con dos fases de movilización de movimientos sociales cada
cual con sus propias características.

Estas dos fases de protesta son el movimiento anti-globalización
alrededor del cambio de milenio y el movimiento de las plazas de 2011.
Estos dos movimientos de protesta comparten muchas similaridades, al
punto que algunas activistas ven al segundo como continuación del
primero.  Al mismo tiempo estas olas han expresado orientaciones
ideológicas diferentes que reflejan el cambio en la situación social y
política desde el arribo de la crisis económica del 2008 y por lo tanto,
se convierten en casos de estudios interesantes para un análisis
comparativo.  Mientras la ideología dominante del movimiento
anti-globalización era anarco-autonomista (o autonomista para
sintetizar) en tanto combinación del anarquismo y el autonomismo, el
movimiento de las plazas se ha caracterizado por la influencia del
populismo de izquierda [@gerbaudo-2018].  Este giro ideológico en los
movimientos sociales tiene un correlato con el cambio en la orientación
tecno-política de los movimientos sociales: el ciber-autonomismo en la
primera ola y el ciber-populismo en la segunda del activismo digital.

Una periodización ideológica del activismo digital
--------------------------------------------------

La transformación del activismo digital en las últimas décadas puede
esquematizarse como un movimiento desde los márgenes hacia el centro de
la arena política, desde una política contra-cultural de resistencia a
una política contra-hegemónica de movilización popular.  Entonces,
mientras las formas tempranas de activismo digital concebían la Internet
como un espacio contra-cultural separado, la segunda ola del activismo
digital la aborda como parte de un _mainstream_ político a ser ocupado
por protestantes [@gerbaudo-2015].  Por lo tanto, las primeras consideran
la Internet un santuario en el que las activistas pueden encontrar
respiro del caracter opresivo de la sociedad.  En cambio, las segundas
consideran la Internet como una pieza fundamental de la sociedad
contemporánea, donde se manifiestan las mismas contradicciones, pero
también donde las activistas tienen la esperanza de desarrollar un
proceso de movilización masiva capaz de atraer, no solo a las personas
altamente politizadas, sino también a una sección significativa de la
población general.

Mi comprensión de la evolución del activismo digital y la presencia de
las dos olas es cercana a la de Karatzogianni, una académica de medios
que ha estado trabajando sobre el activismo digital desde los 2000.
Karatzogianni propone la existencia de cuatro
olas [-@karatzogianni-2015].  La primera va desde el '94 al 2001 y
coincide con la fase temprana del movimiento anti-globalización, desde
el levantamiento zapatista en México en el '94 a las protestas en Génova
que fueron violentamente aplastadas por la policía durante el 2001.  La
segunda fase va desde el 2001 hasta el 2007 y comprende la segunda fase
del movimiento anti-globalización y su prominencia como movimiento
político.  Describe la tercera como la "difusión del activismo digital"
refiriéndose a su migración hacia países del sur global, por fuera de
Europa y Estados Unidos donde se desarrolló originalmente.  La cuarta y
última fase es cuando el activismo digital invade la política
_mainstream_, con el auge de _Wikileaks_, los levantamientos de la
primavera árabe, las revelaciones de Snowden, poniéndolo en el centro de
los conflictos políticos y dejando de ser un fenómeno marginal.

Sin embargo, mi análisis es más simple y solo se enfoca en dos fases
principales.  Explica la transformación como resultado de cambios
ideológicos, que a su vez reflejan cambios en la situación
socio-política, opiniones y actitudes.  Poner el foco en la ideología no
significa negar el rol que juegan los factores tecnológicos, en
particular el giro de la web 1.0 basada en sitios estáticos hacia la web
2.0 con sus redes sociales.  Más bien sugiere que el impacto tecnológico
no puede ser entendido desde una perspectiva meramente instrumental,
sino que necesita abarcar una comprensión del cambio cultural que es
facilitado e influenciado por la tecnología, pero no reducible a ella.
Este abordaje puede aplicarse a las dos fases diferentes que se han
identificado en este análisis: el movimiento anti-globalización y el
movimiento de las plazas.

El movimiento anti-globalización se desarrolló alrededor del cambio de
milenio y se manifestó en una serie de protestas de gran escala contra
las instituciones ecónomicas globales como el Banco Mundial, la
Organización Mundial del Comercio y las reuniones del G8.  Fue un
movimiento multifacético que abarcó corrientes ideológicas dispares como
sindicatos, grupos troskistas, ambientalistas, ONGs tercermundistas y
organizaciones religiosas.  Sin embargo, su núcleo y especialmente las
facciones más jóvenes estaban formados profundamente en el autonomismo y
el anarco-autonomismo, una ideología híbrida inspirada en el movimiento
anarquista y marxista posterior al '68 y marcada por un fuerte espíritu
anti-autoritario y anti-estatista.  Esta ideología se enfocaba en un
proyecto político autónomo, alejándose del estado y el mercado para
intentar construir un espacio auto-gobernado de "lo común".  El
movimiento de las plazas se ha volcado en cambio hacia el populismo de
izquierda, o más específicamente hacia una tendencia del populismo que
llamo _ciudadanismo_, es decir un populismo ciudadano, antes que un
populismo del pueblo.  Esta ideología se centra en una recuperación
desde las bases de la democracia y las instituciones políticas por
partes de las ciudadanas comunes, empezando por reunirse en el espacio
público y los _social media_.  Anhela la construcción de una democracia
radical que permita una participación más auténtica que la que ofrecen
las corruptas instituciones democráticas liberales.

Como veremos, esta oposición entre anarco-autonomismo y populismo es
similar a la oposición entre ciber-autonomismo y ciber-populismo como
orientaciones tecno-políticas dominantes de las dos olas del activismo
digital.  La forma en que las activistas conciben y utilizan la Internet
refleja su cosmovisión general, su actitud frente al estado, la política
y la población general, con sus opiniones y actitudes prevalentes.

Anti-globalización y ciber-autonomismo
--------------------------------------

Comencemos por el movimiento anti-globalización y su activismo digital.
Las activistas anti-globalización persiguieron lo que puede llamarse una
estrategia "ciber-autonomista" que veía a la Internet como un espacio
donde construir islas de resistencia por fuera del control del estado y
el capital.  Como el nombre sugiere, esta lógica comunicacional se
revolvía en la idea de crear espacios autónomos de comunicacion dentro
de la Internet, fuera de una sociedad controlada por el capital y el
estado.  Como propuse anteriormente [@gerbaudo-2014], las activistas
estaban convencidas de que la construcción de infraestructura de
comunicación autónoma era una condición fundamental para una
comunicación alternativa genuina [-@gerbaudo-2014].  Basándose en la
tradición de los medios alternativos de los '60 a los '80, en un
contexto de prensa _underground_, cultura del _fanzine_ y radios
piratas, las activistas técnicas esperaban usar la Internet para romper
el monopolio de los medios informativos corporativos, responsables de
canalizar propaganda neoliberal y el silenciamiento de cualquier punto 
de vista alternativo.  Esta visión fundaba un conjunto de iniciativas 
de medios alternativos que se desarrollaron a finales de los '90 y 
comienzos de los 2000 [@pickard-2006].

La manifestacion más visible de esta estrategia fue _Indymedia_, la
primera iniciativa global de noticias alternativas con decenas de nodos
editoriales repartidos por el mundo.  En el pico de las protestas
anti-cumbres, _Indymedia_ se convirtió en la voz no-oficial aunque
semi-oficial del movimiento anti-globalización y también constituyó una
infraestructura organizacional fundamental para las manifestantes, con
nodos editoriales actuando a menudo como colectivas políticas
directamente involucradas en la organización de las campañas de
protesta.  Además de _Indymedia_, los servicios alternativos de Internet
como _RiseUp_, _Aktivix_ y _Autistici/Inventati_ proveían las
necesidades de comunicación interna del movimiento.  Estos grupos
proveyeron cuentas de correo electrónico personales seguras así como
listas de correo que permitían conversaciones agrupadas por temas, desde
organización de protestas a ocupas y permacultura.  El imaginario
subyacente a estas actividades era el de "islas en la red", como
expresaba el nombre de uno de los proveedores de Internet más
importantes de Italia.  Las activistas pensaban la Internet como zonas
autónomas temporales descritas por Hakim Bey, un espacio que comprendía
islas temporales de un archipiélago rebelde fuera del control del estado
y el capital.  La Internet era concebida como un espacio autónomo donde
el movimiento podía encontrar un lugar solidario para desarrollar
acciones frente al ofrecido por la sociedad consumista dominada por la
hegemonía neoliberal.  Esta es la razón por la que la actitud
tecno-política de esta fase también era fuertemente contra-cultural.
Veía a la Internet como un espacio donde cultivar una cultura
alternativa, claramente diferente de la cultura mayoritaria del momento,
considerada irremediablemente corrupta.  Podría considerarse que el
movimiento de las plazas es la inversa de esta posición.

El movimiento de las plazas y el ciber-populismo
------------------------------------------------

El activismo digital del movimiento de las plazas se caracteriza en
cambio por una orientación tecno-política que he descrito como
"ciber-populista" [-@gerbaudo-2014].  Con esto defino una orientación
tecno-política que considera la web de masas compuesta por servicios de
Internet comerciales controlados por corporaciones monopólicas como
_Facebook_, _Google_ y _Twitter_, como un espacio que a pesar de sus
sesgos capitalistas inherentes necesita ser reapropiado por el activismo
y cuya capacidad de alcance masivo necesita ser domada y utilizada para
otros fines.  Antes que crear una Internet alternativa --un espacio
comunicacional libre, auto-gobernado y no-comercial-- las activistas
técnicas contemporáneas están más preocupadas por domar las capacidades
de alcance de las plataformas de redes sociales corporativas como
_Facebook_ y _Twitter_ y la cultura popular digital que ha emergido en
ellas.

Los ejemplos de esta tendencia ciber-populista abundan en la ola de
protestas del 2011, desde la página de _Facebook_ de Kullena Khaled Said
en Egipto llamando a que cientos de miles de personas tomen las calles,
al trabajo de las activistas en España, Grecia, Estados Unidos, Turquía
y Brasil, que usaron los _social media_ como un medio de movilización
masiva.  En lugar de intentar crear espacios alternativos, las
activistas digitales dentro de estos movimientos intentaron ocupar el
_mainstream_ digital, apropiándose de los _social media_ como
plataformas del pueblo.

Esta estrategia lleva la marca de la ambición popular y mayoritaria de
la ola _Ocuppy_ y el hecho de que estos nuevos movimientos no se
contentan con la construcción de espacios minoritarios de resistencia.
Al utilizar las plataformas de redes sociales corporativas las
activistas invaden los espacios que saben, no les pertenecen y sobre el
cual tienen poco control, pero lo hacen persuadidas de que es necesario
tomarlos para construir formas de movilización popular a medida de las
condiciones técnicas de nuestra era.  En lugar de apuntar a crear zonas
autónomas temporales en la Internet como sus predecesoras, la nueva
generación de activistas digitales desean romper los guetos y reconectar
con el 99% de la población por la que luchan.  Podría describirse esta
posición como más "oportunista" por cuanto intenta explotar las
oportunidades políticas que se desenvuelven en un espacio moralmente
ambiguo por su subordinación a la lógica del mercado.  Sin embargo, este
elemento también ha permitido a estos movimientos ser exitosos y lograr
una magnitud movilizatoria que evidentemente supera la alcanzada por las
activistas anti-globalización.

Conclusión
----------

Para comprender la transformación del activismo digital es necesario
prestar atención no solo a los cambios en la materialidad de la
tecnología, sino también a los factores culturales, sociales y políticos
que dan forma a su comprensión y uso.  Por eso resulta imperativo
recuperar la noción de ideología, entendida como el sistema de creencias
y valores que informan la cosmovisión activista en cualquier período
histórico.

Como he demostrado en este artículo, las diferencias entre la primera ola
de activismo digital que se dio en el cambio de milenio y la segunda
entre el 2000 y el 2010, no solo han seguido la forma de la
transformación de la tecnología digital y el giro de la web 1.0 a las
plataformas de redes sociales de la web 2.0, sino también por cambios en
la ideología de estos movimientos conectados, en particular el cambio
del anarco-autonomismo del movimiento anti-globalización hacia el
populismo del movimiento de las plazas.  Este giro ideológico se ha
traducido, en el contacto del activismo digital, en un giro del
ciber-autonomismo hacia el ciber-populismo, dos orientaciones
tecno-políticas con diferentes asunciones sobre el rol de la tecnología
digital tanto como medios y como espacios de lucha.  Mientras el
ciber-autonomismo concibe la tecnología digital como un espacio autónomo
separado del estado y el capital, el ciber-populismo la concibe como un
espacio de encuentro y movilización popular.

Esta interpretación ideológica del activismo digital no ignora el rol
que juega la tecnología al dar forma a la acción colectiva.  El
activismo digital ciertamente refleja la naturaleza de las capacidades
tecnológicas.  Por ejemplo, el proceso de masificación de la web que se
dio en paralelo a la difusión de los _social media_ explicaría el giro
desde una lógica minoritaria a una mayoritaria de movilización en el
activismo digital.  Sin embargo, la transformación tecnológica no es el
factor determinante.  Sus efectos en el contenido del activismo son
filtrados por narrativas ideológicas y cosmovisiones que contribuyen a
dar forma al modo en que las activistas conciben la Internet como un
campo de lucha político, un aspecto que puede capturarse en la noción de
"orientaciones tecno-políticas" utilizada en este artículo.

Lo que resulta necesario es por lo tanto investigación que pueda dar
mejor cuenta de las formas complejas en las que la ideología da forma a
las prácticas activistas y su contenido.  Esta perspectiva permitiría
superar algunas de las superficialidades en las que incurren muchos de
los análisis contemporáneos del activismo digital y abordar mejor
la forma en que este activismo refleja los temas, actitudes y
motivaciones de los movimientos sociales conectados, aparte de los
factores tecnológicos.

Sobre el autor
--------------

Paolo Gerbaudo es un teórico político y cultural estudiando la
transformación de los movimientos sociales y los partidos políticos en
la era digital.  Es el director del Centro por la Cultura Digital en el
_King's College_ de Londres y el autor de _Tweets and the Streets:
Social Media and Contemporary Activism_ \[Tuits y las calles: Los
_social media_ y el activismo contemporáneo\] (Pluto, 2012) y del
próximo a publicarse _The Mask and the Flag: Populism, Citizenism and
Global Protest_ \[La máscara y la bandera: populismo, ciudadanismo y
protesta global\] (Hurts/OUP, 2017).

Bibliografía
------------
